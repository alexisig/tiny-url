import os
import random
import shutil

from string import  ascii_uppercase, ascii_lowercase, digits
from datetime import datetime

from models.tiny_url import TinyUrl
from models.url import Url

TINY_URL_SIZE = 8
STORE_RELATIVE_PATH = 'src/store'


def check_if_file_exists(file_path: str) -> bool:
    return os.path.exists(file_path)


def check_if_folder_exists(folder_path: str) -> bool:
    return os.path.isdir(folder_path)


def get_url_list_from_file_path(file_path: str) -> list[Url]:
    with open(file_path, 'r') as f:
        urls = f.readlines()

    return [Url(path=url.replace('\n', '')) for url in urls]


def check_if_uid_is_already_used(uid: str) -> bool:
    return os.path.exists(f'{STORE_RELATIVE_PATH}/{uid}')


def get_tiny_url_unique_id() -> str:
    char_set = ascii_uppercase + ascii_lowercase + digits
    uid = ''.join(random.sample(char_set * TINY_URL_SIZE, TINY_URL_SIZE))

    if check_if_uid_is_already_used(uid):
        return get_tiny_url_unique_id()
    
    return uid


def get_tiny_url_from_url(url: Url) -> TinyUrl:
    return TinyUrl(
        path=url.path,
        short=get_tiny_url_unique_id(),
        timestamp=datetime.now().timestamp()
    )


def create_store_folder() -> None:
    os.mkdir(STORE_RELATIVE_PATH)


def remove_store() -> None:
    shutil.rmtree(STORE_RELATIVE_PATH, ignore_errors=True)


def save_tiny_url_to_store(tiny_url: TinyUrl) -> str:
    output_path = f'{STORE_RELATIVE_PATH}/{tiny_url.short}'

    with open(output_path, 'w') as f:
        f.write(tiny_url.path)
        f.write('\n')
        f.write(str(tiny_url.timestamp))
    
    return output_path

def get_tiny_urls_from_store() -> list[TinyUrl]:
    tiny_urls = []

    for filename in os.listdir(STORE_RELATIVE_PATH):
        file_path = f'{STORE_RELATIVE_PATH}/{filename}'
        with open(file_path, 'r') as f:
            path, timestamp = f.readlines()
            tiny_urls.append(TinyUrl(
                path=path.replace('\n', ''),
                short=filename,
                timestamp=float(timestamp)
            ))
    
    return tiny_urls


def get_seconds_in_minutes(seconds: float) -> float:
    return seconds / 60 


def remove_entry(tiny_url: TinyUrl):
    file_path = f'{STORE_RELATIVE_PATH}/{tiny_url.short}'
    os.remove(file_path)


def remove_old_entries():
    tiny_urls = get_tiny_urls_from_store()

    time_now = datetime.now()

    for tiny_url in tiny_urls:
        tiny_url_datetime = datetime.fromtimestamp(
            tiny_url.timestamp
        )
        created_since = time_now - tiny_url_datetime 
        time_in_minutes = get_seconds_in_minutes(
            created_since.total_seconds()
        )
        older_than_a_minute = time_in_minutes > 1

        if older_than_a_minute:
            remove_entry(tiny_url)


