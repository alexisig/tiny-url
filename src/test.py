from unittest import TestCase, main

from utils import get_url_list_from_file_path

class TinyUrlTestCase(TestCase):
    def test_list_of_urls_loads_properly(self):
        file_path = 'src/data/list1.txt'

        url_list = get_url_list_from_file_path(file_path)
        expected_size_list = 100

        self.assertEqual(
            len(url_list),
            expected_size_list,
            'The list doesnt load the expected amount of urls'
        )

if __name__ == '__main__':
    main()