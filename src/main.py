"""
Main entry of the program.

CLI tool that creates tiny url.
"""

from argparse import ArgumentParser

from utils import get_tiny_url_from_url, \
    get_url_list_from_file_path, \
    remove_store, \
    save_tiny_url_to_store, \
    check_if_file_exists, \
    check_if_folder_exists, \
    create_store_folder, \
    remove_old_entries, \
    STORE_RELATIVE_PATH


PARSER_DESCRIPTION = 'Creates a tiny url from a list of URL in a text file'
FILE_PATH_HELP = 'The file containing a list of urls to read from'
FULL_CLEAN_HELP = 'Clean the store entirely before running'
CLEAN_OLD_HELP = 'Clean old entries in the store before running'

parser = ArgumentParser(description=PARSER_DESCRIPTION)

parser.add_argument(dest='file_path', type=str, help=FILE_PATH_HELP)
parser.add_argument('--full-clean', help=FULL_CLEAN_HELP,
                    action='store_true', default=False)
parser.add_argument('--clean-old', help=CLEAN_OLD_HELP,
                    action='store_true', default=False)

args = parser.parse_args()
file_path = args.file_path
full_clean = args.full_clean
clean_old = args.clean_old

if not check_if_file_exists(file_path):
    raise Exception('The requested file input does not exist.')

if check_if_folder_exists(STORE_RELATIVE_PATH) and full_clean:
    remove_store()

if not check_if_folder_exists(STORE_RELATIVE_PATH):
    create_store_folder()

if clean_old and not full_clean:
    remove_old_entries()

url_list = get_url_list_from_file_path(file_path)

for url in url_list:
    tiny_url = get_tiny_url_from_url(url)
    ouput_path = save_tiny_url_to_store(tiny_url)
