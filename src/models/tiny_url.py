from datetime import datetime
from typing import NamedTuple


class TinyUrl(NamedTuple):
    path: str
    short: str
    timestamp: float