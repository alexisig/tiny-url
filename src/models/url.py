from typing import NamedTuple


class Url(NamedTuple):
    path: str