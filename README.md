# Tiny URL

## Install

```properties
python3 -m venv env
source env/bin/active
pip install -r requirements.txt
```

## Example

```properties
python src/main.py src/data/list1.txt
```

## Options

```
usage: main.py [-h] [--full-clean] [--clean-old] file_path

positional arguments:
  file_path     The file containing a list of urls to read from

optional arguments:
  -h, --help    show this help message and exit
  --full-clean  Clean the store entirely before running
  --clean-old   Clean old entries in the store before running
```

## Questions

### Combinations

```
What is the maximum number of URL’s you can store with 8 alphanum chars, upper and lowercase? Suppose that in the English language, there are 26 alphabetic chars. Digits are arabic numerals 0-9.
```

According to [calculator.net](https://www.calculator.net/permutation-and-combination-calculator.html?cnv=62&crv=8&x=57&y=5) there are 
`3,381,098,545` possile values.

### Collisions

```
Do you see any collisions on your program? (Same URL different tiny URL’s, different URL’s same tiny URL’s?, spot the bugs here)
```

Yes, it is possible that the randomly generated values generate the same uid for different urls, as it is not related to the value of the url (like a hash would be). To avoid this issue, I am checking whether the value already exists before storing it and run the `get_uid` command recursively until one is available.


### Scaling and refactoring

```
Can you refactor? What are your approaches for refactoring? Say you want to answer the next question.
Does your program scale well? Can it deal with a million requests per
second? What are the bottlenecks on your approach?
```
I would ideally do the following:
- add an asyncio loop running indefinitely with planned cron events to do the cleaning operations
- add an external api to insert data in the store
- parralelize the tiny url creations
- use a proper database

I thought of a hash based solutions (triming the hash to 8 characters), but after some reading found that the chance of collisions were too high (https://stackoverflow.com/questions/30561096/chance-of-a-duplicate-hash-when-using-first-8-characters-of-sha1).


The I/O performance of the program is fine for a small amount of data, but it will quickly show itself insuficient. The current bottlenecks are:
- constantly opening and closing files
- recursively checking for collisions

### Cleanup

```
Can you put a timestamp to the couple URL - tiny URL so as to clean up
after a given amount of time?
```

This would be best if the program was running in an infinite loop (say an asyncio loop), but I've made this execute at the start of the CLI tool (using the `--clean-old` option).